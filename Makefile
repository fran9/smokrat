
SHELL=/bin/bash

PHONY: serve build

serve:
	cd site/ && hugo serve 

build:
	cd site/ && hugo && mv public/ ..
